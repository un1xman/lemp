import os
def replace_func(oldstring, newstring):
    with open("/etc/php-fpm.d/www.conf") as pfile:
        newtext = pfile.read().replace(oldstring, newstring)
        with open("/etc/php-fpm.d/www.conf", "w") as pfile:
            pfile.write(newtext)
replace_func(oldstring="user = apache", newstring="user = nginx")
replace_func(oldstring="group = apache", newstring="group = nginx")
replace_func(oldstring=";listen.owner = nobody", newstring="listen.owner = nginx")
replace_func(oldstring=";listen.group = nobody", newstring="listen.group = nginx")
replace_func(oldstring=";listen.mode = 0666", newstring="listen.mode = 0666")
with open ("/etc/php-fpm.d/www.conf", "a") as plisten:
    plisten.write("listen = /run/php-fpm/web.sock" + '\n')

os.system("chown -R nginx:nginx /var/lib/php")
os.system("systemctl restart php-fpm")
os.system("systemctl restart php-fpm >> /dev/null 2>&1")
os.system("systemctl enable php-fpm >> /dev/null 2>&1")


