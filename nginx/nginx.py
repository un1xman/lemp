import os
import urllib.request

root_dir: str = input("Enter your root directory: ")
server_name: str = input("Enter your server name(default un1xman.local) ")

os.system("echo TestWebpage >> {}/index.html".format(root_dir))
os.system("mkdir -p {}".format(root_dir))
# os.system("cp ")
print ("Now we create test index.html file")
os.system("chown -R nginx:nginx {}".format(root_dir))
index_file = root_dir + "/index.html"
with open(index_file, 'w') as f1:
    f1.write('Test WebPage ')
print("Created 'Test WebPage/n' html page")
print("==================================")
print("Congigure Selinux for our web server")
os.system("semanage fcontext -a -t httpd_sys_content_t {}".format(root_dir))
os.system("setsebool -P httpd_enable_homedirs true".format(root_dir))
os.system("restorecon -r {}".format(root_dir))
os.system("chmod -R 755 {}".format(root_dir))
print("======================================")
print("")
print("Replace root folder to nginx configure file")

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
default_conf = SCRIPT_DIR + "/un1xman.conf"
print(default_conf)


def replace_func(old, new):
    with open("{}".format(default_conf)) as pfile:
        newtext = pfile.read().replace(old, new)
        with open("{}".format(default_conf), "w") as pfile:
            pfile.write(newtext)

replace_func(old="/usr/share/nginx/codes", new=root_dir)
replace_func(old="un1xman.local", new=server_name)
os.system("cp {} /etc/nginx/conf.d/".format(default_conf))

print("===========================================================")
print("Open http service on firewall and restart nginx service")
print("========================================================")
os.system("systemctl restart nginx")
os.system("firewall-cmd --add-service=http --permanent")
os.system("firewall-cmd --reload")
print("***********************Finish**********************************")
#web_chk = urllib.request.urlopen("http://localhost").getcode()

#if web_chk == 200:
#    print("Nginx is okay... Test html page working succesfully ")
#    print("Finish  script.")
#    print("Bye!")
#else :
#        print("We have problem")
#'''
