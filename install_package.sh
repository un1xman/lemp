clear
echo "***********WELCOME TO MY SCRIPT***********"
echo "STEP 1. Install nginx packages"
echo "NGINX INSTALLING....."
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
echo "****************************************************************"
rpm_path=$(echo "$SCRIPT_DIR/files/nginx-1.15.8-1.el7_4.ngx.x86_64.rpm")
rpm -ivh $rpm_path 2>>  error.log > /dev/null
ng_ex=$(ls /etc | grep nginx)
if [ "$ng_ex" == "" ]
then
        echo "OOOPS"
        echo "We have a problem...Check error.log file"
	exit 1
else
        echo "Nginx installed with succesfully"
fi
echo "==========================================================================="
echo "STEP 2. Install php packages"
yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm 2>> /var/log/lemp/error.log >> /dev/null 
yum-config-manager --enable remi-php72 2>> /var/log/lemp/error.log >> /dev/null
yum install -y php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-fpm php-mysqlnd 2>> /var/log/lemp/error.log >> /dev/null

which php >> /dev/null 2>&1
pc=$?
if [ $pc -eq 0 ]
then
        echo "PHP7.2 HAS BEEN INSTALLED IN YOUR SYSTEM"
else
        echo "We have problem.Check error.php log file"
	exit 1
fi
echo "============================================================================"
echo "STEP 3. Install mariadb packages"
yum install mariadb mariadb-server -y 2> /var/log/lemp/error.log >> /dev/null
echo "============================================================================"
echo "STEP 4. Install phpmyadmin packages"
yum install phpmyadmin -y 2> error.log >> /dev/null
echo "============================================================================"
echo ""
echo "************************** Now we must start nginx php-fpm mariadb service **************************"
systemctl start nginx && systemctl enable nginx 2>> /var/log/lemp/error.log >> /dev/null 
ng_srv=$(systemctl status nginx | grep "active (running)")
if [ "$ng_srv" != "" ]
then
        echo "Nginx service is start and added to startup"
        echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
else
        echo "Nginx has been not started"
        echo "Look nginx_error.log file"
	exit 1
fi
systemctl start mariadb 2>> /var/log/lemp/error.log >> /dev/null
systemctl enable mariadb 2>> /var/log/lemp/error.log >> /dev/null

mar=$(systemctl status mariadb | grep "active (running)")
if [ "$mar" != "" ]
then
        echo "Mariadb service is start and added to startup"
        echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
else
        echo "Mariadb has been not started"
        echo "Look error.log file"
	exit 1
fi

#yum install php-fpm -y >> /dev/null 2>&1
systemctl start php-fpm >> /dev/null 2>&1
systemctl enable php-fpm >> /dev/null 2>&1

pf_srv=$(systemctl status php-fpm | grep "active (running)")
if [ "$pf_srv" != "" ]
then
        echo "php-fpm service is start and added to startup"
        echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
else
        echo "php-fpm has been not started"
        echo "Look error.php file"
	exit 1
fi

