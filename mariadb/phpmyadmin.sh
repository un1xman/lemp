yum install phpmyadmin -y 2> error.log >> /dev/null
root_folder=$(cat /etc/nginx/conf.d/un1xman.conf | grep "root " | awk '{print $2}' | sed 's/[;]//g')

mkdir "$root_folder/phpMyAdmin/"
cp -r usr/share/phpMyAdmin/* "$root_folder/phpMyAdmin/"
systemctl restart nginx
echo "Open on web browser http://server-ip/phpMyAdmin"
