echo "FIRST INSTALL REQUIREMENTS PACKAGES FOR THIS SCRIPT"
echo " This might take several minutes..."
echo " "
mkdir /var/log/lemp
LEMP_DIR="$( cd "$( dirname "$0" )" && pwd )"
mv "$LEMP_DIR/deinstall.sh /tmp
yum install -y epel-release 2>> /var/log/lemp/error.log >> /dev/null
yum install -y wget 2>> /var/log/lemp/error.log >> /dev/null
yum install policycoreutils-python -y 2>> /var/log/lemp/error.log >> /dev/null
echo "Finish installing packages..."
echo"============================================="
clear
echo" Now begin configuration LEMP server"
echo "##############################################################################"
LEMP_DIR="$( cd "$( dirname "$0" )" && pwd )"
bash "$LEMP_DIR/install_package.sh"
python3.6 "$LEMP_DIR/nginx/nginx.py"
python3.6 "$LEMP_DIR/php/php.py"
bash "$LEMP_DIR/mariadb/mysql_secure.sh"
bash "$phpmyadmin.sh"

echo "Finish.Bye" 
