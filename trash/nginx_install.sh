#!/bin/bash
clear

echo "PREPARING INSTALL NGINX...."
echo "NGINX INSTALLING....."
SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
echo $SCRIPT_DIR
echo $rpm_path
echo "***********************************"
rpm_path=$(echo "$SCRIPT_DIR/nginx-1.15.8-1.el7_4.ngx.x86_64.rpm")
rpm -ivh $rpm_path 2>  error.log > /dev/null
ng_ex=$(ls /etc | grep nginx)
if [ "$ng_ex" == "" ]
then
	echo "OOOPS"
	echo "We have a problem...Check error.log file"
else
	echo "NGINX INSTALLED WITH SUCCESFULLY"
fi
echo "================================================================="
echo "Now start and enable nginx service"
systemctl start nginx && systemctl enable nginx >> /dev/null 2>&1
ng_srv=$(systemctl status nginx | grep "active (running)")
if [ "$ng_srv" != "" ]
then
        echo "Nginx is start and added to startup"
        echo "Finish script...Everythink is OK"
else
        echo "Nginx has been not started"
        echo "Look nginx_error.log file"
fi
#while  read -p "Do you have a epel repo in your system? please answer yes no: " answer
#do
#    if [ "$answer" == "Y" ] || [ "$answer" == "YES" ] || [ "$answer" == "y" ] || [ "$answer" == "yes" ]
#    then
#	echo "PREPARING INSTALL NGINX...."
#	echo "NGINX INSTALLING....."
#        yum clean all >> /dev/null 2>&1
#        yum repolist >> /dev/null 2>&1
#        yum install nginx -y >> /dev/null 2>&1
#	which nginx >> /dev/null 2>&1
#	rc=$?
#	if [ $rc -eq 0 ]
#	then
#		echo "NGINX INSTALLED WITH SUCCESFULLY"
#		echo "================================"
#		break
#	else 
#		echo "We have a problem.Check "install.log" file"
#		echo "Bye!"		echo "ERROR: Nginx not installed Check epel repository exisiting or your internet connection" >> install.log
#		exit 1
#	fi
#	
#   elif [ "$answer" == "N" ] || [ "$answer" == "n" ] || [ "$answer" == "NO" ] || [ "$answer" == "no" ] 
#    then
#	echo "INSTALLING EPEL RELEASE..."
#	echo "=========================="
#        yum install epel-release -y >> /dev/null 2>&1
#	chk_epel=$(ls /etc/yum.repos.d/ | grep "epel")
#        if [ "$chk_epel" == "" ]
#        then
#                echo "We have a problem...Check "install.log" file"
#                echo "ERROR: Epel soft not downloading.Check wget command exist or internet option" >> install.log
 #               exit 1
 #       else
#		echo "PREPARING INSTALL NGINX...."
#	        echo "NGINX INSTALLING....."
#	        yum clean all >> /dev/null 2>&1
#                yum repolist >> /dev/null 2>&1
#                yum install nginx -y >> /dev/null 2>&1
#		which nginx >> /dev/null 2>&1
#		rc=$?
#        	if [ $rc -eq 0 ]
#        	then
#                	echo "NGINX INSTALLED WITH SUCCESFULLY"
#                	echo "================================"
#			break
#        	else
#                	echo "We have a problem.Check "install.log" file"
#                	echo "Bye!"
#                	echo "ERROR: Nginx not installed Check epel repository exisiting or your internet connection" >> install.log
#			exit 1
#		fi
#        fi
#    else
#        echo Please write correct variant 'Y','yes','YES','y','NO','n','no','N'
#        continue
#    fi
#done
#echo "Now start and enable nginx service"
#systemctl start nginx && systemctl enable nginx >> /dev/null 2>&1
#ng_srv=$(systemctl status nginx | grep "active (running)")
#if [ "$ng_srv" != "" ]
#then
#	echo "Nginx is start and added to startup"
#	echo "Finish script...Everythink is OK"
#else
#	echo "Nginx has been not started"
#	echo "Look nginx_error.log file"
#fi
