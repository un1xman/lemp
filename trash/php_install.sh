
clear
yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm >> /dev/null 2>&1
echo "REMI-RPM INSTALLED"
yum-config-manager --enable remi-php72 >> /dev/null 2>&1
yum install -y php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysqlnd >> /dev/null 2>&1
echo "PHP PACKAGES INSTALLED"
which php >> /dev/null 2>&1
pc=$?
if [ $pc -eq 0 ]
then
	echo "PHP7.2 HAS BEEN INSTALLED IN YOUR SYSTEM"
else
	echo "We have problem.Check error.php log file"
fi
yum install php-fpm -y >> /dev/null 2>&1
systemctl start php-fpm >> /dev/null 2>&1
systemctl enable php-fpm >> /dev/null 2>&1

pf_srv=$(systemctl status php-fpm | grep "active (running)")
if [ "$pf_srv" != "" ]
then
        echo "php-fpm service is start and added to startup"
        echo "Finish script...Everythink is OK"
else
        echo "php-fpm has been not started"
        echo "Look error.php file"
fi
