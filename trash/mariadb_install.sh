yum install mariadb mariadb-server -y 2> error.log >> /dev/null
systemctl start mariadb 2> error.log >> /dev/null
systemctl enable mariadb 2> error.log >> /dev/null

mar=$(systemctl status mariadb | grep "active (running)")
if [ "$mar" != "" ]
then
        echo "Mariadb service is start and added to startup"
        echo "Finish script...Everythink is OK"
else
        echo "Mariadb has been not started"
        echo "Look error.log file"
fi
