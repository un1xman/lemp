yum remove -y nginx
yum remove -y php*
yum remove -y mariadb mariadb-server
rm -rf /var/www
rm -rf /etc/nginx /var/log/nginx /var/cache/nginx /usr/share/nginx
rm -rf /var/lib/php /usr/bin/php /usr/lib64/php /usr/share/php
rm -rf /etc/yum.repos.d/re*
rm -rf /etc/yum.repos.d/epel*
rm -rf /var/log/lemp

